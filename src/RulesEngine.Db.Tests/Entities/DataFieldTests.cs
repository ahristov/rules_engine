﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using RulesEngine.Domain.Entities;
using System.Threading.Tasks;

namespace RulesEngine.Db.Tests.Entities
{
    [TestFixture]
    public class DataFieldTests: BaseTestFixture
    {
        [Test]
        public async Task CanWriteAndRead()
        {
            var dataField = new DataField { Name = "age", DataPath = "user.age", DataType = DataTypeEnum.Integer };
            Ctx.DataFields.Add(dataField);
            Ctx.SaveChanges();

            var count = await Ctx.DataFields.CountAsync();
            count.Should().BeGreaterThan(0);

            var dataFieldFromDb = await Ctx.DataFields.FirstOrDefaultAsync(x => x.Name == "age");
            dataFieldFromDb.Should().NotBeNull();
            dataFieldFromDb.Id.Should().BeGreaterThan(0);
            dataFieldFromDb.Name.Should().Be(dataField.Name);
            dataFieldFromDb.DataPath.Should().Be(dataField.DataPath);
            dataFieldFromDb.DataType.Should().Be(dataField.DataType);
        }
    }
}
