﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using NUnit.Framework;
using System.Threading.Tasks;

namespace RulesEngine.Db.Tests
{
    public abstract class BaseTestFixture
    {
        private static readonly DbContextOptions<RulesDbContext> _sqliteOptions = new DbContextOptionsBuilder<RulesDbContext>()
            .UseSqlite("DataSource=:memory:")
            .Options;

        private static readonly DbContextOptions<RulesDbContext> _inMemoOptions = new DbContextOptionsBuilder<RulesDbContext>()
            .UseInMemoryDatabase("RulesEngine")
            .Options;

        private static readonly bool _useSqLite = false;

        protected RulesDbContext Ctx;

        [SetUp]
        protected async Task BaseSetUp()
        {
            Ctx = new RulesDbContext(_useSqLite ? _sqliteOptions : _inMemoOptions);
            await SetUpDatabase();
        }

        [TearDown]
        protected async Task BaseTearDown()
        {
            await CleanUpDatabase();
            Ctx.Dispose();
            Ctx = null;
        }

        private async Task SetUpDatabase()
        {
            if (_useSqLite)
            {
                var dbCreator = Ctx.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
                await dbCreator.EnsureCreatedAsync();
                await dbCreator.CreateTablesAsync();
            }
        }

        private async Task CleanUpDatabase()
        {
            if (_useSqLite)
            {
                var dbCreator = Ctx.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
                await dbCreator.EnsureDeletedAsync();
            }
        }

    }
}
