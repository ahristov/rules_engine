﻿using Microsoft.EntityFrameworkCore;

namespace RulesEngine.Db.Tests
{
    public class RulesDbContextInMemory : RulesDbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseInMemoryDatabase("RulesEngine");
    }
}
