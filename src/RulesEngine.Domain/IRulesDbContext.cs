﻿using RulesEngine.Domain.Entities;
using System.Collections.Generic;

namespace RulesEngine.Domain
{
    public interface IRulesDbContext
    {
        IEnumerable<Condition> Conditions { get; }
        IEnumerable<DataField> DataFields { get; }
        IEnumerable<OutputVariable> OutputVariables { get; }
        IEnumerable<Rule> Rules { get; }
        IEnumerable<RuleSet> RuleSets { get; }
        IEnumerable<RuleSetRule> RuleSetRules { get; }
        IEnumerable<RuleSetVersion> RuleSetVersions { get; }
    }
}
