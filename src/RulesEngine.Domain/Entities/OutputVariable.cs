﻿namespace RulesEngine.Domain.Entities
{
    public class OutputVariable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OutputVariableModifiersEnum  Modifiers { get; set; }
        public string Value { get; set; }
    }
}
