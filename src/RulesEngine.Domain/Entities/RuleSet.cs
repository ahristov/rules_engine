﻿namespace RulesEngine.Domain.Entities
{
    public class RuleSet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descr { get; set; }
    }
}
