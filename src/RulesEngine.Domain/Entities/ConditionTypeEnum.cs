﻿namespace RulesEngine.Domain.Entities
{
    public enum ConditionTypeEnum: int
    {
        NotSpecified = 0,
        Equal = 1,
        GreaterThan = 2,
        GreaterOrEqual = 3,
        LessThan = 4,
        LessOrEqual = 5,
        OneOf = 6,
        RegExp = 7,
    }
}
