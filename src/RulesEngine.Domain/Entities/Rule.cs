﻿using System.Collections.Generic;

namespace RulesEngine.Domain.Entities
{
    public class Rule
    {
        public int Id { get; set; }
        public DataField DataField { get; set; }
        public string Descr { get; set; }
        public bool AnyCondition { get; set; }
        public bool AnyChildren { get; set; }
        public ICollection<Rule> SubRules { get; set; }
        public ICollection<Condition> Conditions { get; set; }
    }
}
