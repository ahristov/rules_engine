﻿using System;
using System.Collections.Generic;

namespace RulesEngine.Domain.Entities
{
    public class RuleSetRule
    {
        public int Id { get; set; }
        public RuleSetVersion RuleSetVersion { get; set; }
        public Rule Rule { get; set; }
        public int Ord { get; set; }
        public bool Disabled { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public bool ContinueOnSuccess { get; set; }
        public bool StopOnFailure { get; set; }
        public ICollection<OutputVariable> OutputVariables { get; set; }
    }
}
