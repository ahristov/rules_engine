﻿using System;

namespace RulesEngine.Domain.Entities
{
    public class RuleSetVersion
    {
        public int Id { get; set; }
        public RuleSet RuleSet { get; set; }
        public string Descr { get; set; }
        public DateTime StartDt { get; set; }
    }
}
