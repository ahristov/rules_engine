﻿using System;

namespace RulesEngine.Domain.Entities
{
    [Flags]
    public enum OutputVariableModifiersEnum : int
    {
        NotSpecified = 0,
        SetOnSuccess = 1,
        SetOnFailure = 2,
        AddOnSuccess = 4,
        AddOnFailure = 8,
        RemoveOnSuccess = 16,
        RemoveOnFailure = 32,
        ClearOnSuccess = 64,
        ClearOnFailure = 128,
    }
}
