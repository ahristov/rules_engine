﻿namespace RulesEngine.Domain.Entities
{
    public class Condition
    {
        public int Id { get; set; }
        ConditionTypeEnum ConditionType { get; set; }
        public string Value { get; set; }
        public bool Negate { get; set; }

    }
}
