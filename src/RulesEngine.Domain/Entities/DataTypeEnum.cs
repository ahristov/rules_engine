﻿namespace RulesEngine.Domain.Entities
{
    public enum DataTypeEnum: int
    {
        NotSpecified = 0,
        String = 1,
        Integer = 2,
        Decimal = 3,
        DateTime = 4,
    }
}
