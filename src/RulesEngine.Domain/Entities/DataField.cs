﻿using System;

namespace RulesEngine.Domain.Entities
{
    public class DataField
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DataPath { get; set; }
        public DataTypeEnum DataType { get; set; }
    }
}
