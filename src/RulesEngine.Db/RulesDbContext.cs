﻿using Microsoft.EntityFrameworkCore;
using RulesEngine.Domain;
using RulesEngine.Domain.Entities;
using System.Collections.Generic;

namespace RulesEngine.Db
{
    public class RulesDbContext : DbContext, IRulesDbContext
    {
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<DataField> DataFields { get; set; }
        public DbSet<OutputVariable> OutputVariables { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<RuleSet> RuleSets { get; set; }
        public DbSet<RuleSetRule> RuleSetRules { get; set; }
        public DbSet<RuleSetVersion> RuleSetVersions { get; set; }

        IEnumerable<Condition> IRulesDbContext.Conditions => Conditions;
        IEnumerable<DataField> IRulesDbContext.DataFields => DataFields;
        IEnumerable<OutputVariable> IRulesDbContext.OutputVariables => OutputVariables;
        IEnumerable<Rule> IRulesDbContext.Rules => Rules;
        IEnumerable<RuleSet> IRulesDbContext.RuleSets => RuleSets;
        IEnumerable<RuleSetRule> IRulesDbContext.RuleSetRules => RuleSetRules;
        IEnumerable<RuleSetVersion> IRulesDbContext.RuleSetVersions => RuleSetVersions;

        public RulesDbContext() { }
        public RulesDbContext(DbContextOptions<RulesDbContext> options)
            : base(options)
        { }
    }
}
